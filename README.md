# gshape 
**gshape** is a library for the processing of geographical data.   
It's implementation is in Python 3 (module *pygshapes*).  
Currently it is in alpha phase.

## Summary

It is inspired to the multiple dispatch concepts as exemplified by the Julia language.  
Even if Python has only single dispatch, multiple dispatch can still be simulated by additional type conditions on the 
successive function arguments.  
The class methods mainly provide the instances attributes, while the operations are achieved through 
dispatches over generic functions.  
Metaclasses provide the templates to apply for the concrete geometric classes.
All shapes derive from the Shape metaclass, that provides a set of standard methods:
- space dimension
- center
- length
- area


### Current status

2020-10-04: project start  
2020-10-04: partial implementation for generic, n-dimensional Point class 
2020-11-14: working implementations for Segment and Line; operator functions; basic Vector implementation







