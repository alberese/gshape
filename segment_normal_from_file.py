
import sys

from pygshapes.utils.gis_io.vectorial import *
from pygshapes.shapes.operators.transformations.rotations import *


# definition parameters

source_data_pth = "/home/mauro-grigio/Documents/projects/gshape/test_data/faults_32633_subset.shp"
output_data_path = r"/home/mauro-grigio/Documents/projects/gshape/test_data/faults_32633_subset_normals.json"
normal_length = 500


# program

if __name__ == '__main__':

    success, result = try_read_as_geodataframe(source_data_pth)

    if not success:
        msg = result
        print(msg)
        sys.exit()

    lines_gdf = result

    geometries = lines_gdf.geometry

    arrays = []
    for geometry in geometries:
        xs, ys = geometry.xy
        arrays.append(np.transpose(np.vstack([xs, ys])))

    lines = []
    for array in arrays:
        lines.append(Line(array))

    normal_segments = []
    for line_ndx, line in enumerate(lines):

        print(f"Processing line: {line_ndx}")

        for segment_ndx, segment in enumerate(line):

            normal_segments.append(segment_normal(
                segment,
                normal_length
                )
            )

        print(" - processed")

    print("Try creating segments dataframe")

    success, result = try_create_segments_json(
        segments=normal_segments
    )

    if not success:
        msg = result
        print(f"Error: {msg}")
        sys.exit()

    normals_json = result

    with open(output_data_path, "w") as ofile:
        ofile.writelines(normals_json)

    print(f"Saved geojson file as {output_data_path}")

