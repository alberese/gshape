

from pygshapes.shapes.primitives.line import *
from pygshapes.shapes.operators.transformations.rotations import *


def calculate_segment_normal(
        segment: Segment,
        length: numbers.Real
) -> Segment:

    segment_center = segment.center()

    normal_segment = vrotate(
        segment,
        angle=90,
        center=True
    )

    left_vector = normal_segment.as_vector()
    scaled_left_vector = left_vector.set_magnitude(length/2.0)
    scaled_right_vector = scaled_left_vector.invert()

    scaled_left_segment = Segment.from_vector(segment_center, scaled_left_vector)
    scaled_right_segment = Segment.from_vector(segment_center, scaled_right_vector)

    return Segment(scaled_left_segment.end_pt, scaled_right_segment.end_pt)



length = 10

line = Line([[0, 0], [1, 1], [2, 2], [3, 3]])

for segment in line:

    print(segment)

    normal_segment = calculate_segment_normal(
        segment,
        length
    )

    print(f"Normal: {normal_segment} with length: {normal_segment.length()}")


