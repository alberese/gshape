
import numbers

import numpy as np


class Vector(np.ndarray):
    """
    Vector
    Modified from Vec3 in apsg.

    """

    def __new__(cls,
                arr: np.ndarray
                ):

        obj = np.asarray(arr).view(cls)

        return obj

    def __abs__(self) -> numbers.Real:
        """
        Calculates the vector magnitude.

        Examples:
        >>> abs(Vector([1, 1, 1]))
        1.7320508075688772
        """

        return np.linalg.norm(self)

    def set_magnitude(self,
                      value: numbers.Real
                      ) -> 'Vector':
        """
        Scales a vector to a provided length.

        :param value: the length to apply to the vector
        :return: scaled vector

        Examples:
        >>> Vector([1, 1, 1]).set_magnitude(2)
        Vector([1.15470054, 1.15470054, 1.15470054])
        """

        return Vector(value * self / abs(self))

    def invert(self) -> 'Vector':
        """
        Returns the inverted vector.

        :return: the inverted vector

        Examples:
        >>> Vector([0, 2, -2]).invert()
        Vector([ 0, -2,  2])
        """

        return Vector(-self)


if __name__ == "__main__":

    import doctest
    doctest.testmod()


