
from typing import Optional

import numbers

from pygshapes.shapes.defaults import *
from pygshapes.vectors import *
from pygshapes.shapes.primitives.shape import *
from pygshapes.shapes.operators.relationships.distances import *
from pygshapes.utils.types import *


class Point(np.ndarray, Shape):
    """
    Generic Point class.
    Inspired to the Vec3 class in apsg by Ondrej Lexa.
    """

    def __new__(cls, arr):

        return np.asarray(arr).view(cls)

    @classmethod
    def from_scalars(cls,
                     *vals
                     ) -> 'Point':
        """
        Returns a Point from a list of values.

        :param vals:
        :return:

        Examples:
          >>> Point.from_scalars(0, 0)
          Point(0.0000, 0.0000)
        """

        return cls(
            arr=np.array(vals)
        )

    def __repr__(self) -> str:
        """
        Represents a point instance.

        :return:

        Examples:
          >>> Point(np.array([0, 0]))
          Point(0.0000, 0.0000)
          >>> Point(np.array([0, 1, 0]))
          Point(0.0000, 1.0000, 0.0000)
        """

        values = [f"{val:.4f}" for val in self]
        return f"Point({', '.join(values)})"

    def space_dimension(self) -> numbers.Integral:
        """
        Returns the embedding spatial dimention of the point instance.

        Examples:
          >>> Point(np.array([0, 0])).space_dimension()
          2
        """

        return len(self)

    def center(self) -> 'Point':

        return Point(arr=self)

    def length(self) -> numbers.Real:

        return 0.0

    def area(self) -> numbers.Real:

        return 0.0

    @property
    def x(self) -> Optional[numbers.Real]:

        if self.space_dimension() >= 1:
            return self[0]
        else:
            return None

    @property
    def y(self) -> Optional[numbers.Real]:

        if self.space_dimension() >= 2:
            return self[1]
        else:
            return None

    @property
    def z(self) -> Optional[numbers.Real]:

        if self.space_dimension() >= 3:
            return self[2]
        else:
            return None

    def asVector(self
                 ) -> Vector:
        """
        Convert a point to a vector.

        :return:

        Examples:
          >>> Point(np.array([0, 0])).asVector()
          Vector([0, 0])
        """

        return Vector(self)

    def asAntiVector(self
                 ) -> Vector:
        """
        Convert a point to the opposite vector.

        :return:

        Examples:
          >>> Point(np.array([1, 1, 4])).asAntiVector()
          Vector([-1, -1, -4])
        """

        return Vector(-self)

    def is_coincident(self,
                      another: 'Point',
                      tolerance: numbers.Real = MIN_SEPARATION_THRESHOLD
                      ) -> bool:
        """
        Check spatial coincidence of two points, limiting to the horizontal (XY) plane.

        :param another: the point to compare.
        :type another: Point.
        :param tolerance: the maximum allowed distance between the two points.
        :type tolerance: numbers.Real.
        :return: whether the two points are coincident.
        :rtype: bool.
        :raise: Exception.

        Example:

        """

        check_type(another, "Second point", Point)

        return dist(self, another) <= tolerance


if __name__ == "__main__":

    import doctest
    doctest.testmod()
