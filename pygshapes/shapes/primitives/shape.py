

import abc


class Shape(object, metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def space_dimension(self):
        """Calculate shape space dimension"""

    @abc.abstractmethod
    def center(self):
        """Calculate shape center"""

    @abc.abstractmethod
    def length(self):
        """Calculate shape length"""

    @abc.abstractmethod
    def area(self):
        """Calculate shape area"""


