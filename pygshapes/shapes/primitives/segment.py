
from typing import Tuple

from pygshapes.shapes.operators.relationships.distances import *
from pygshapes.shapes.operators.conversions.clones import *
from pygshapes.shapes.operators.relationships.mean import *


class Segment(Shape):
    """
    Generic Segment class.
    """

    def __init__(self,
                 start_pt: Point,
                 end_pt: Point
                 ):
        """
        Creates a segment instance.

        :param start_pt: the start point.
        :param end_pt: the end point.
        """

        if end_pt.space_dimension() != start_pt.space_dimension():
            raise Exception(f"Start point has dimension {start_pt.space_dimension()} while end point has dimension {end_pt.space_dimension()}")
        if dist(start_pt, end_pt) == 0.0:
            raise Exception("Start and end points cannot be coincident")

        self._start_pt = clone(start_pt)
        self._end_pt = clone(end_pt)

    def __repr__(self) -> str:
        """
        Represents a Segment instance.

        :return: the Segment representation.


        Examples:
          >>> Segment(Point.from_scalars(0, 0), Point.from_scalars(1, 0))
          Segment([0 0], [1 0])
        """

        return f"Segment({self.start_pt}, {self.end_pt})"

    def space_dimension(self) -> numbers.Integral:

        return self._start_pt.space_dimension()

    def center(self) -> 'Point':
        """
        Calculates the center of a segment.

        :return: the segment center

        Examples:
        >>> Segment(Point([-1, 0]), Point([1, 0])).center()
        Point(0.0000, 0.0000)
        >>> Segment(Point.from_scalars(1, -1), Point.from_scalars(3, 1)).center()
        Point(2.0000, 0.0000)
        """

        return mean(self.points)

    def length(self) -> numbers.Real:

        return dist(
            self._start_pt,
            self._end_pt
        )

    def area(self) -> numbers.Real:

        return 0.0

    @property
    def start_pt(self) -> Point:

        return clone(self._start_pt)

    @property
    def end_pt(self) -> Point:

        return clone(self._end_pt)

    @property
    def points(self) -> Tuple[Point, Point]:

        return self.start_pt, self.end_pt

    def axis_lengths(self) -> np.ndarray:
        """
        Calculates the lengths along the different axes.

        :return:

        Examples:
        >>> Segment(Point([-1, 0]), Point([1, 0])).axis_lengths()
        array([2, 0])
        """

        return np.array(self.end_pt - self.start_pt)

    def as_vector(self) -> Vector:
        """
        Create a vector with the same orientation and length as the segment.
        
        :return:

        Examples:
        >>> Segment(Point([-1, 0]), Point([1, 0])).as_vector()
        Vector([2, 0])
        """

        return Vector(self.axis_lengths())

    @classmethod
    def from_vector(cls,
                    point: Point,
                    vector: Vector
                    ) -> 'Segment':
        """
        Creates a segment from a starting point and a vector.

        :param point: start point
        :param vector: vector
        :return: the resulting segment

        Examples:
        >>> Segment.from_vector(Point([1, 1, 1]), Vector(np.array([3, 2, 1])))
        Segment([1 1 1], [4 3 2])
        """

        return cls(point, point + vector)

    def invert(self) -> 'Segment':
        """
        Invert the segment.

        :return: the inverted segment
        """

        return Segment(self.end_pt, self.start_pt)


if __name__ == "__main__":

    import doctest
    doctest.testmod()

