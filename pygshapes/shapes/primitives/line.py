
import numbers

from pygshapes.shapes.primitives.segment import *


class Line(np.ndarray, Shape):
    """
    Line class, inheriting from Numpy ndarray and pygshapes Shape.
    Inspired to the Vec3 class in apsg by Ondrej Lexa.
    """

    def __new__(cls, arr):

        return np.asarray(arr).view(cls)

    def __repr__(self) -> str:
        """
        Represents a Line instance as a shortened text.

        :return: a textual shortened representation of a Line instance.

        Examples:
          >>> Line(np.array([[0, 0], [1, 0]]))
          Line with 2 points: [0 0] ... [1 0]
        """

        num_points = self.num_pts()

        if num_points == 0:
            txt = "Empty Line"
        else:
            if num_points == 1:
                txt = f"Line with single point: {self.start_pt}"
            else:
                txt = f"Line with {self.num_pts()} points: {self.start_pt} ... {self.end_pt}"

        return txt

    def area(self) -> numbers.Real:
        """

        :return:

        Examples:
          >>> Line(np.array([[0, 0], [1, 0]])).area()
          0.0
          >>> Line(np.array([[0, 0, 0], [1, 0, 1]])).area()
          0.0
        """

        return 0.0

    def space_dimension(self) -> numbers.Integral:
        """

        :return:

        Examples:
          >>> Line(np.array([[0, 0], [1, 0]])).space_dimension()
          2
          >>> Line(np.array([[0, 0, 0], [1, 0, 1]])).space_dimension()
          3
        """

        return self.shape[1]

    def lengths(self) -> np.ndarray:
        """
        The lengths of the individual line segments.

        :return:

        Examples:
          >>> Line(np.array([[0,0], [1, 0]])).lengths()
          array([1.])
          >>> Line(np.array([[0,0], [1, 0], [3, 0]])).lengths()
          array([1., 2.])
        """

        d = np.diff(np.array(self), axis=0)
        p = np.power(d, 2)
        s = np.sum(p, axis=1)
        return np.sqrt(s)

    def incremental_lengths(self) -> np.ndarray:
        """
        The cumulative lengths of the line segments.

        :return:

        Examples:
          >>> Line(np.array([[0,0], [1, 0]])).incremental_lengths()
          array([1.])
          >>> Line(np.array([[0,0], [1, 0], [3, 0]])).incremental_lengths()
          array([1., 3.])
        """

        return np.cumsum(self.lengths())

    def length(self) -> numbers.Real:
        """
        The total line length.

        :return:

        Examples:
          >>> Line(np.array([[0,0], [1, 0]])).length()
          1.0
          >>> Line(np.array([[0,0], [1, 0], [3, 0]])).length()
          3.0
        """

        return np.sum(self.lengths())

    def num_pts(self) -> numbers.Integral:
        """
        The number of the line points.

        :return:

        Examples:
          >>> Line(np.array([[0,0], [1, 0]])).num_pts()
          2
          >>> Line(np.array([[0,0], [1, 0], [3, 0]])).num_pts()
          3
          >>> Line(np.array([[0, 0, 1], [1, 0, 4], [3, 0, 2], [1, 7, 4]])).num_pts()
          4
        """

        return self.shape[0]

    def num_segments(self) -> numbers.Integral:
        """
        Return segment number.

        :return: the number of line segments

        Examples:
        >>> Line(np.array([[0, 0], [1, 1], [2, 2], [3, 3]])).num_segments()
        3
        >>> Line(np.array([[0, 0]])).num_segments()
        0
        """

        return self.num_pts() - 1

    def pt(self,
           ndx: numbers.Integral
           ) -> Point:
        """
        Returns the Point instance at given index.

        :param ndx: the point index
        :return:

        Examples:
          >>> Line(np.array([[0, 0], [1, 0]])).pt(0)
          Point(0.0000, 0.0000)
          >>> Line(np.array([[0, 0, 0], [1, 0, 4], [7, 1.2, 3.22]])).pt(2)
          Point(7.0000, 1.2000, 3.2200)
        """

        return Point(self[ndx])

    @property
    def start_pt(self) -> Optional[Point]:
        """
        Return the first point of a Line or None when no points.

        :return: the first point or None.

        Examples:
          >>> Line(np.array([[7, 1.2, -2.04], [1, 0, 4], [7, 1.2, 3.22]])).start_pt
          Point(7.0000, 1.2000, -2.0400)
        """

        return self.pt(0) if self.num_pts() > 0 else None

    @property
    def end_pt(self) -> Optional[Point]:
        """
        Return the first point of a Line or None when no points.

        :return: the first point or None.
        Examples:
          >>> Line(np.array([[7, 1.2, -2.04], [1, 0, 4], [7, 4.9, 3.22]])).end_pt
          Point(7.0000, 4.9000, 3.2200)
        """

        return self.pt(self.num_pts() - 1) if self.num_pts() > 0 else None

    def segment(self,
        ndx: numbers.Integral
    ) -> Optional[Segment]:
        """
        Returns the optional segment at index ndx.

        :param ndx: the segment index.
        :type ndx: numbers.Integral
        :return: the optional segment
        :rtype: Optional[Segment]
        """

        if ndx > self.num_pts() - 2:
            return None

        return Segment(
            start_pt=self.pt(ndx),
            end_pt=self.pt(ndx + 1)
        )

    def __iter__(self):
        """
        Return each element of a Line, i.e., its segments.

        Examples:
        >>> line = Line([[0, 0], [1, 1], [2, 2], [3, 3]])
        >>> for segment in line: print(segment)
        Segment([0 0], [1 1])
        Segment([1 1], [2 2])
        Segment([2 2], [3 3])
        """

        return (self.segment(i) for i in range(self.num_pts()-1))


if __name__ == "__main__":

    import doctest
    doctest.testmod()
