
from typing import List

from pygshapes.shapes.primitives.point import *


def mean(
        points: List[Point]
) -> Point:
    """Mean points center.

    Examples:
      >>> mean([Point.from_scalars(-1,0), Point.from_scalars(1,0)])
      Point(0.0000, 0.0000)
      >>> mean([Point.from_scalars(0,5), Point.from_scalars(0,-5)])
      Point(0.0000, 0.0000)
      >>> mean([Point.from_scalars(5,5), Point.from_scalars(-5,-5)])
      Point(0.0000, 0.0000)
      >>> mean([Point.from_scalars(6,6), Point.from_scalars(-4,-4)])
      Point(1.0000, 1.0000)
      >>> mean([Point.from_scalars(4,7), Point.from_scalars(-6,-3)])
      Point(-1.0000, 2.0000)
      """

    return Point(
        np.mean(
            np.vstack(points),
            axis=0
        )
    )


if __name__ == "__main__":

    import doctest
    doctest.testmod()
