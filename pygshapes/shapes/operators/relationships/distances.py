
from functools import singledispatch

from pygshapes.shapes.primitives.point import *


@singledispatch
def dist(
        shape1: Shape,
        shape2: Shape
) -> numbers.Real:
    """
    Distance between two shapes.

    :param shape1: first shape
    :param shape2: second shape
    :return: the shapes distance
    """

    return NotImplemented


@dist.register(Point)
def _(
        shape1: Point,
        shape2: Point
) -> numbers.Real:
    """Point distance

    :param shape1: first point
    :param shape2: second point
    :return: the points distance

    Examples:
      >>> dist(Point.from_scalars(-1,0), Point.from_scalars(1,0))
      2.0
      >>> dist(Point.from_scalars(0,2), Point.from_scalars(0,-2))
      4.0
    """

    if type(shape2) == Point:
        return np.linalg.norm(shape1 - shape2)
    else:
        return NotImplemented


if __name__ == "__main__":

    import doctest
    doctest.testmod()
