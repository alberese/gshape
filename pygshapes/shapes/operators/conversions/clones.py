
from functools import singledispatch

from pygshapes.shapes.primitives.point import *


@singledispatch
def clone(
        shape: Shape
) -> Shape:
    """
    Clone a shape.

    :param shape: shape to clone
    :return: cloned shape
    """

    return NotImplemented


@clone.register(Point)
def _(
        point: Point
) -> Point:
    """
    Clone a Point instance.

    :param point: point to clone
    :return: the point clone
    """

    return Point(arr=point)


if __name__ == "__main__":

    import doctest
    doctest.testmod()

