
from functools import singledispatch

from affine import Affine

from pygshapes.shapes.primitives.point import *
from pygshapes.shapes.primitives.segment import *
from pygshapes.shapes.primitives.line import *
from pygshapes.shapes.operators.transformations.translations import *


@singledispatch
def vrotate(
        shape: Shape,
        angle: numbers.Real,
        center: bool = False
) -> Shape:
    """
    Rotates a shape counter-clockwise.

    :param shape: the shape to rotate
    :param angle: the rotation angle, decimal degrees, counter-clockwise
    :param center: whether to rotate around the shape center
    :return: the rotate shape
    """

    return NotImplemented


@vrotate.register(Point)
def _(
        point: Point,
        angle: numbers.Real,
        center: bool = False
) -> Point:
    """Rotates a point counter-clockwise.
    TODO: generate a D-agnostic result.

    Examples:
      >>> vrotate(Point.from_scalars(1,0), angle=90)
      Point(0.0000, 1.0000)
      >>> vrotate(Point.from_scalars(1,0), angle=180)
      Point(-1.0000, 0.0000)
      >>> vrotate(Point.from_scalars(1,0), angle=90, center=True)
      Point(1.0000, 0.0000)
    """

    if center:
        return clone(point)
    else:
        xr, yr = Affine.rotation(angle) * (point.x, point.y)
        return Point.from_scalars(xr, yr)


@vrotate.register(Segment)
def _(
        segment: Segment,
        angle: numbers.Real,
        center: bool = False
) -> Segment:
    """Rotates a segment counter-clockwise.
    TODO: generate a D-agnostic result.

    Examples:
      >>> vrotate(Segment(Point.from_scalars(0, 0), Point.from_scalars(1, 0)), angle=90)
      Segment([0. 0.], [0. 1.])
      >>> vrotate(Segment(Point.from_scalars(0, 0), Point.from_scalars(1, 0)), angle=45)
      Segment([0. 0.], [0.70710678 0.70710678])
      >>> vrotate(Segment(Point.from_scalars(1, 0), Point.from_scalars(2, 0)), angle=90)
      Segment([0. 1.], [0. 2.])
      >>> vrotate(Segment(Point.from_scalars(1, 0), Point.from_scalars(2, 0)), angle=90)
      Segment([0. 1.], [0. 2.])
      >>> vrotate(Segment(Point.from_scalars(1, -1), Point.from_scalars(3, 1)), angle=180, center=True)
      Segment([3. 1.], [ 1. -1.])
    """

    start_point = segment.start_pt
    end_point = segment.end_pt

    if center:

        center_pt = segment.center()
        start_point = Point(start_point - center_pt)
        end_point = Point(end_point - center_pt)

    rot_start_point = vrotate(start_point, angle)
    rot_end_point = vrotate(end_point, angle)

    if center:
        rot_start_point = rot_start_point + center_pt
        rot_end_point = rot_end_point + center_pt

    return Segment(rot_start_point, rot_end_point)


def segment_normal(
        segment: Segment,
        length: numbers.Real
) -> 'Segment':
    """
    Calculates segment normal as a new segment.

    :param length: the length of the normal segment to produce
    :return: the normal segment
    """

    segment_center = segment.center()

    normal_segment = vrotate(
        segment,
        angle=90,
        center=True
    )

    left_vector = normal_segment.as_vector()
    scaled_left_vector = left_vector.set_magnitude(length/2.0)
    scaled_right_vector = scaled_left_vector.invert()

    scaled_left_segment = Segment.from_vector(segment_center, scaled_left_vector)
    scaled_right_segment = Segment.from_vector(segment_center, scaled_right_vector)

    return Segment(scaled_left_segment.end_pt, scaled_right_segment.end_pt)


if __name__ == "__main__":

    import doctest
    doctest.testmod()

