
from functools import singledispatch

from pygshapes.shapes.primitives.segment import *
from pygshapes.vectors import *


@singledispatch
def shift(
        shape: Shape,
        vector: Vector
) -> Shape:
    """
    Shift a shape.

    :param shape: the shape to shift
    :param vector: the shift vector
    :return: the shifted shape
    """

    return NotImplemented


@shift.register(Point)
def _(
        point: Point,
        vector: Vector
) -> Point:
    """Shift a point by a provided vector.

    Examples:
      >>> shift(Point.from_scalars(1, 2, 0), vector=Vector(np.array([0, 4, -2])))
      Point(1.0000, 6.0000, -2.0000)
      >>> shift(Point.from_scalars(1, 2, 0), vector=Vector(np.array([-1, 7, -0.2])))
      Point(0.0000, 9.0000, -0.2000)
    """

    return Point(point + vector)


if __name__ == "__main__":

    import doctest
    doctest.testmod()

